import React from 'react';
import FacebookLogin from 'react-facebook-login';
import InstagramLogin from 'react-instagram-login';
import logo from './logo.svg';
import './App.css';

const responseFacebook = (response) => {
  console.log("Facebook Response is", response);
}

const componentClicked = () => {
  console.log('Component Clicked');
}

const responseInstagram = (response) => {
  console.log("Instagram Response", response);

  const data = new FormData();
  data.append("client_id", "e2de0838433542a08411351567a54dae");
  data.append("", "\\");
  data.append("client_secret", "26323b34f9ce4d14b049ae4f3edbe63f");
  data.append("", "\\");
  data.append("grant_type", "authorization_code");
  data.append("", "\\");
  data.append("redirect_uri", "https://facebook-instagram-login.herokuapp.com/");
  data.append("", "\\");
  data.append("code", response);
  data.append("", "\\");

  console.log("Form Data is https://api.instagram.com/oauth/access_token"+data);

  // fetch("https://api.instagram.com/oauth/access_token", {
  //   method: 'POST',
  //   headers: {
  //     "Content-Type": "multipart/form-data"
  //   },
  //   body : data
  // })
  // .then(instagram => instagram.json())
  // .then(data => console.log({ data }));
}
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <FacebookLogin
          appId="549267839141720"
          autoLoad={true}
          fields="name,email,picture"
          onClick={componentClicked}
          callback={responseFacebook} 
        />
        <br/>
        <InstagramLogin
          clientId="e2de0838433542a08411351567a54dae"
          buttonText="Login"
          onSuccess={responseInstagram}
          onFailure={responseInstagram}
        />
      </header>
    </div>
  );
}

export default App;
